﻿using Android.App;
using Android.Widget;
using Android.OS;
using Facebook;
using System.Collections.Generic;

namespace Facebookpostingapp
{
    [Activity(Label = "Facebookpostingapp", MainLauncher = true, Icon = "@drawable/icon")]
    public class MainActivity : Activity
    {
        //app id 1590485284588946
        //app secret 07eaab4be1c57b51b8a4ac4e3e190f68

            // Get your own App ID at developers.facebook.com/apps
        const string FacebookAppId = "1590485284588946";


        // You must get this token authorizing by either using Facebook App or a WebView.
        // Please review included samples.
        string userToken = "EAAWmibZAojZAIBAOK0QmZCcFq7utFmZBqY6lPPTUuMpZBGyz3febInOcv4Ewm80wZA6QUD6zB5JYZADIuEZBeQF8FwdFXQbt0sZCleP3lZAZBo5FmzXISZCzugFnSFAyMW0W581Cuzvnvf7lOOyqogyqJOLdBvBZAMVUITDhPDzi7ZC6hMEQZDZD";

        protected override void OnCreate(Bundle bundle)
        {
            base.OnCreate(bundle);

            // Set our view from the "main" layout resource
            SetContentView(Resource.Layout.Main);
            TextView message = FindViewById<TextView>(Resource.Id.txtMessage);
            message.Text = "Hi This is Riyaz";
            Button button = FindViewById<Button>(Resource.Id.btnPost);
            button.Click += Button_Click;

            
        }

        private void Button_Click(object sender, System.EventArgs e)
        {
            PostToMyWall();
           // GetMyInfo();
            TextView post = FindViewById<TextView>(Resource.Id.txtPost);
           post.Text = "Message posted on your wall!";


        }

        void PostToMyWall()
        {
            FacebookClient fb = new FacebookClient(userToken);
            string myMessage = "Hello from Xamarin";

            fb.PostTaskAsync("me/feed", new { message = myMessage }).ContinueWith(t => {
                if (!t.IsFaulted)
                {
                    string message = "Great, your message has been posted to you wall!";
                   System.Console.WriteLine(message);
                }
            });
        }

        void GetMyInfo()
        {
            // This uses Facebook Graph API
            // See https://developers.facebook.com/docs/reference/api/ for more information.
            FacebookClient fb = new FacebookClient(userToken);

            fb.GetTaskAsync("me").ContinueWith(t => {
                if (!t.IsFaulted)
                {
                    var result = (IDictionary<string, object>)t.Result;
                    //var result = (IDictionary<string, object>)t.Result;
                    string myDetails = string.Format("Your name is: {0} {1} and your Facebook profile Url is: {3}",
                                                      (string)result["first_name"], (string)result["last_name"],
                                                      (string)result["link"]);
                    // System.Console.WriteLine(myDetails);
                    TextView post = FindViewById<TextView>(Resource.Id.txtPost);
                    post.Text = myDetails;

                }
            });
        }
    }
}

